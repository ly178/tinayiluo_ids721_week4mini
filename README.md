# Containerized Rust Actix Web Service
[![pipeline status](https://gitlab.com/ly178/tinayiluo_ids721_week4mini/badges/main/pipeline.svg)](https://gitlab.com/ly178/tinayiluo_ids721_week4mini/-/commits/main)

## Overview

This project focuses on containerizing a simple Rust Actix web service that delivers a "Quote of the Day" to users. The application is built using Rust's powerful Actix web framework, and the goal is to deploy this application within a Docker container, ensuring that it can be run in any environment with Docker installed. This README provides a comprehensive guide on how to set up, build, and run the application, along with the steps to containerize it using Docker.

## Requirements

- Rust programming environment (Cargo, Rustc)
- Docker installed on your machine
- Basic understanding of web development and containerization concepts

## Getting Started

### Step 1: Project Setup

1. Create a new Rust project:
    ```sh
    cargo new <project-name>
    ```
2. Add Actix-web and other necessary dependencies to your `Cargo.toml`.

### Step 2: Application Development

1. Implement the web service logic in `src/main.rs` to return a random quote from a predefined list of quotes.
2. Design a basic front-end webpage (`index.html`) under `static/root` that allows users to retrieve the quote of the day.

### Step 3: Testing Locally

1. Run the web application using `cargo run`. This will start the web server.
2. Open a web browser and navigate to `localhost:50505` to ensure the application runs correctly.

### Step 4: Containerization with Docker

1. Create a `Dockerfile` in the root directory of your project to specify how the Docker image should be built.
2. Build the Docker image:
    ```sh
    docker build -t <web-app-name> .
    ```
3. Run the Docker container:
    ```sh
    docker run -d -p 50505:50505 <image-name>
    ```

### Step 5: CI/CD Automation (Optional)

- Implement a `Makefile` or another CI/CD tool to automate the testing, building, and deployment processes.

## Deliverables

### Quote of the Day Web App

![Screen_Shot_2024-02-18_at_4.34.03_PM](/uploads/b3392bb1cfd430c08d9c245ca2674ecc/Screen_Shot_2024-02-18_at_4.34.03_PM.png)

![Screen_Shot_2024-02-18_at_4.34.49_PM](/uploads/f55aed86e9cf64e31c9a80c016cf65fc/Screen_Shot_2024-02-18_at_4.34.49_PM.png)

### Docker Image 

![Screen_Shot_2024-02-18_at_5.32.49_PM](/uploads/af1a4643faa852e8edc87b638ef475cd/Screen_Shot_2024-02-18_at_5.32.49_PM.png)

### Docker Container

![Screen_Shot_2024-02-18_at_5.36.10_PM](/uploads/be924aae6448433ccd24ce6b0a238d7a/Screen_Shot_2024-02-18_at_5.36.10_PM.png)

## Conclusion

This project demonstrates how a Rust Actix web application can be containerized using Docker, making it easily deployable in any environment that supports Docker. By following the steps outlined above, you can containerize and deploy your own Rust-based web services efficiently.

